//
//  ViewController.swift
//  ericPaginationSearchSortPulltoRefresh
//
//  Created by SST Mac Mini on 21/01/22.
//

import UIKit
import Alamofire

struct Welcome: Codable {
    let meta: Meta
    var data: [Datum]
}

// MARK: - Datum
struct Datum: Codable {
    let id, userID: Int
    let title: String
    let dueOn: String
    let status: Status

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case title
        case dueOn = "due_on"
        case status
    }
}

enum Status: String, Codable {
    case completed = "completed"
    case pending = "pending"
}

struct Meta: Codable {
    let pagination: Pagination
}

struct Pagination: Codable {
    let total, pages, page, limit: Int
    let links: Links
}

struct Links: Codable {
    let current, next: String
}


class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    var arrayData : Welcome!
    var userpage = 1
    var isSearch = false
    var searchArray: [Datum] = []
    
    @IBOutlet weak var SearchBar: UISearchBar!
    
    @IBOutlet weak var mytable: UITableView!
    let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        SearchBar.delegate = self
        
        self.getdata()
        refreshControl.attributedTitle = NSAttributedString(string: "Loading...")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        mytable.refreshControl = refreshControl
    }
    @objc func refresh(_ sender: UIRefreshControl) {
        
        self.getdata()
        refreshControl.endRefreshing()
        self.mytable.reloadData()
        
    }
    
    func getdata () {
        
        let url = "https://gorest.co.in/public/v1/todos?page=\(userpage)"
        
        AF.request(url, method: .get).responseJSON { response in
            
            switch response.result {
                
            case .failure(let error):
                print(error.localizedDescription)
                
            case .success:
                
                if let dataToDecode = response.data {
                    
                    do {
                      let data = try JSONDecoder().decode(Welcome.self, from: dataToDecode)
                        
                        if self.userpage == 1 {
                            self.arrayData = data
                        }
                        else {
                            self.arrayData.data.append(contentsOf: data.data)
                        }
                    }
                    catch(let error){
                        print(error.localizedDescription)
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5){
                    self.mytable.reloadData()
                }
                
             }
          
        }
        
    }
    
    
    @IBAction func sort(_ sender: Any) {
        
        let sortedArray = arrayData.data.sorted(by: { $0.title < $1.title })
        
        self.arrayData.data.removeAll()
        
        self.arrayData.data.append(contentsOf: sortedArray)
        
        self.mytable.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchArray = self.arrayData.data.filter{ $0.title.lowercased().contains(searchText.lowercased()) }
        
        if searchText.isEmpty
        {
            self.isSearch = false
        }else
        {
            self.isSearch = true
        }
        
        self.mytable.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
    
        return true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            if searchArray != nil {
                return searchArray.count
            }
        } else {
            if arrayData != nil {
                return arrayData.data.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:PageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "pagecell") as! PageTableViewCell
        
        if isSearch {
            cell.lblid.text = "\(searchArray[indexPath.row].id)"
            cell.lbltitle.text = "\(searchArray[indexPath.row].title)"
        } else{
            cell.lblid.text = "\(arrayData.data[indexPath.row].id)"
            cell.lbltitle.text = "\(arrayData.data[indexPath.row].title)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.arrayData.data.count - 1 {
            
            if userpage < self.arrayData.meta.pagination.pages {
                
                userpage += 1
                
                self.getdata()
                
                let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
                        spinner.startAnimating()
                        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                    
                    self.mytable.tableFooterView = spinner
                    self.mytable.tableFooterView?.isHidden = false
            }
            
        }
        
    }
    
}

